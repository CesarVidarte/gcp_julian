from google.cloud import bigquery


def _create_table(client, table_id):
    # TODO(developer): Set table_id to the ID of the table to create.
    # table_id = "your-project.your_dataset.your_table_name"
    schema=[
        bigquery.SchemaField("Province_State", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("Country_Region", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("Last_Update", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("Confirmed", "INT64", mode="NULLABLE"),
        bigquery.SchemaField("Deaths", "INT64", mode="NULLABLE"),
        bigquery.SchemaField("Recovered", "INT64", mode="NULLABLE"),
        bigquery.SchemaField("source_dt", "STRING", mode="NULLABLE"),
    ]
    table = bigquery.Table(table_id, schema=schema)
    table = client.create_table(table)  # Make an API request.

if __name__ == "__main__":
    client = bigquery.Client()
    table_id = "ejercicio-practica-325921.dataset.data"
    _create_table(client, table_id)