import re
import numpy as np
import pandas as pd
from google.cloud import storage  # pylint: disable=import-error
from google.api_core import page_iterator  # pylint: disable=import-error


def _item_to_value(iterator, item):  # pylint:disable=unused-argument
    return item


def _get_folders(storage_client, bucket_name, prefix):
    extra_params = {
        "projection": "noAcl",
        "prefix": prefix,
        "delimiter": '/'
    }
    path = "/b/" + bucket_name + "/o"
    iterator = page_iterator.HTTPIterator(
        client=storage_client,
        api_request=storage_client._connection.api_request,
        path=path,
        items_key='prefixes',
        item_to_value=_item_to_value,
        extra_params=extra_params,
    )
    return list(iterator)


def _get_paths_to_load(unprocesed_folders, procesed_folders):
    procesed_folders = {folder.replace("UDL", "RDL") for folder in procesed_folders}
    return list(set(unprocesed_folders) - procesed_folders)


def _read_csv(path):
    path = "gs://bucket-practica/" + path
    raw_df = pd.read_csv(path)
    return raw_df


def _process_csv_file(path, tmp_csv_path):
    relabel = {
        "Last Update": "Last_Update",
        "Country/Region": "Country_Region",
        "Lat": "Latitude",
        "Long_": "Longitude",
        "Province/State": "Province_State",
    }
    labels = [
        "Province_State",
        "Country_Region",
        "Last_Update",
        "Confirmed",
        "Deaths",
        "Recovered",
    ]
    raw_df = _read_csv(path)
    for key in relabel.keys():
        if key in raw_df:
            raw_df.rename(columns={key: relabel[key]}, inplace=True)
    for label in labels:
        if label not in raw_df:
            raw_df[label] = np.nan
    filename = re.findall(r"([0-9-]+.csv)", path, re.MULTILINE)[0]
    date = filename[6:10] + "-" + filename[0:2] + "-" + filename[3:5]
    usable_df = raw_df[labels]
    usable_df["source_dt"] = date
    usable_df["file_path"] = path
    usable_df.to_csv(
        tmp_csv_path,
        sep=',',
        header=True,
        index=False,
        mode='w',
        encoding="utf-8"
    )


def _upload_blob(bucket_client, source_file_name, destination_blob_name):
    blob = bucket_client.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)


def _process_files(bucket_client, paths, tmp_csv_path):
    for path in paths:
        iterator = bucket_client.list_blobs(delimiter='/', prefix=path)
        response = iterator._get_next_page_response()
        path = response['items'][0]["name"]
        _process_csv_file(path, tmp_csv_path)
        path = path.replace("RDL", "UDL")
        _upload_blob(bucket_client, tmp_csv_path, path)


def handler(request):  # pylint:disable=unused-argument
    tmp_csvs_path = "/tmp/csv.csv"
    bucket_name = 'bucket-practica'
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    raw_data_folders = _get_folders(storage_client, bucket_name, "RDL/data/covid/")
    usable_data_folders = _get_folders(storage_client, bucket_name, "UDL/data/covid/")
    paths_to_load = _get_paths_to_load(raw_data_folders, usable_data_folders)
    if len(paths_to_load) > 1:
        _process_files(bucket, paths_to_load, tmp_csvs_path)
    return ("success", 200)
