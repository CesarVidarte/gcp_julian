import os
from datetime import datetime
import requests
from google.cloud import storage  # pylint: disable=import-error


def _get_url(url):
    try:
        response = requests.get(url).json()
        return response
    except requests.exceptions.RequestException as exc:
        raise requests.exceptions.RequestException(
            f"There are problems with the URL {type(exc)} {url}, {repr(exc)}"
        )


def _get_list(response):
    url_list = []
    for dicts in response:
        if dicts["name"].endswith(".csv"):
            url_list.append(dicts["download_url"])
    return url_list


def upload_blob(bucket_name, source_file_name, destination_blob_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)


def handler(request):  # pylint:disable=unused-argument
    url = os.environ.get("URL")
    req = _get_url(url)
    csvs = _get_list(req)
    date = datetime.today().strftime('%Y-%m-%d')
    with open("/tmp/file.csv", "w", encoding="utf8") as temp_file:
        for line in csvs:
            temp_file.write("%s\n" % line)
    upload_blob("bucket-practica", "/tmp/file.csv", "RDL/incoming/metadata/source_dt=" + date + "/metadata.csv")
    return ("success", 200)
