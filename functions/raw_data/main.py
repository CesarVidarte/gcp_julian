import re
import pandas as pd
from google.cloud import storage  # pylint: disable=import-error


def _download_bucket_file(bucket_client, file_path, tmp_filename):
    blob = bucket_client.blob(file_path)
    blob.download_to_filename(tmp_filename)


def _read_urls(urls_tmp_path):
    with open(urls_tmp_path, encoding='utf') as file:
        url_list = [path[:-1] for path in file.readlines()]
    return url_list


def _urls_2_df(url):
    df_csv = pd.read_csv(url)
    return df_csv


def _upload_blob(bucket_client, source_file_name, destination_blob_name):
    blob = bucket_client.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)


def _get_files(urls, csv_tmp_path, bucket_client, output_path):
    for url in urls:
        df_csv = _urls_2_df(url)
        filename = re.findall(r"([0-9-]+.csv)", url, re.MULTILINE)[0]
        df_csv.to_csv(csv_tmp_path, sep=',', header=True, index=False)
        file_date = filename[6:10] + "-" + filename[0:2] + "-" + filename[3:5]
        final_output_path = output_path + file_date + "/"
        _upload_blob(bucket_client, csv_tmp_path, final_output_path + filename)


def handler(event, context):  # pylint:disable=unused-argument
    base_folder = "UDL/metadata/source_dt="
    if not event["name"].startswith(base_folder):
        return 0
    execution_dt = re.findall(r"=([0-9-]+)\/", event["name"], re.MULTILINE)[0]
    output_path = "RDL/data/covid/source_dt="
    urls_file = base_folder + execution_dt + "/incremental.csv"
    storage_client = storage.Client()
    bucket = storage_client.get_bucket('bucket-practica')
    urls_tmp_path = "/tmp/urls.csv"
    csv_tmp_path = "/tmp/csv.csv"
    _download_bucket_file(bucket, urls_file, urls_tmp_path)
    urls = _read_urls(urls_tmp_path)
    _get_files(urls, csv_tmp_path, bucket, output_path)
    return 0
