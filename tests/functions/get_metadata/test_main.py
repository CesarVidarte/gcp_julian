import unittest
import requests
import functions.get_metadata.main as gm


class Test_process_csv(unittest.TestCase):
    def test_get_url(self):
        with self.assertRaises(requests.exceptions.RequestException):
            gm._get_url(
                "https://githubom/CSSEGISandData/COVID-19/tree/master/archived_dat"
            )

    def test_get_list(self):
        response = [
            {"name": "a.csv", "download_url": "http1"},
            {"name": "acsv", "download_url": "http2"},
            {"name": "b.csv", "download_url": "http3"},
        ]
        expected = ["http1", "http3"]
        l = gm._get_list(response)
        self.assertEqual(l, expected)
