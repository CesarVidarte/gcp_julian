clean-metadata:
	rm -rf dist/get_metadata

build-metadata: clean-metadata
	mkdir -p dist/get_metadata
	cp functions/get_metadata/* dist/get_metadata

deploy-metadata: build-metadata
	gcloud functions deploy get-metadata --runtime python39 --trigger-http --source dist/get_metadata

run-metadata:
	gcloud functions call get-metadata

clean-check:
	rm -rf dist/check_metadata

build-check: clean-check
	mkdir -p dist/check_metadata
	cp functions/check_metadata/* dist/check_metadata

deploy-check: build-check
	gcloud functions deploy check-metadata --runtime python39 --trigger-resource bucket-practica --trigger-event google.storage.object.finalize --source dist/check_metadata

clean-raw:
	rm -rf dist/raw_data

build-raw: clean-raw
	mkdir -p dist/raw_data
	cp functions/raw_data/* dist/raw_data

deploy-raw: build-raw
	gcloud functions deploy raw-data --runtime python39 --trigger-resource bucket-practica --trigger-event google.storage.object.finalize --source dist/raw_data

clean-processed:
	rm -rf dist/processed_data

build-processed: clean-processed
	mkdir -p dist/processed_data
	cp functions/processed_data/* dist/processed_data

deploy-processed: build-processed
	gcloud functions deploy processed-data --runtime python39 --trigger-http --source dist/processed_data

run-processed:
	gcloud functions call processed-data

clean-local:
	rm -rf ./venv

empty-metadata:
	gsutil rm -r gs://bucket-practica/RDL/data/covid
	gsutil rm -r gs://bucket-practica/RDL/incoming/metadata
	gsutil rm -r gs://bucket-practica/UDL/metadata
	gsutil rm -r gs://bucket-practica/UDL/watermark

setup-local: clean-local
	virtualenv venv
	venv/bin/python -m pip install -r functions/get_metadata/requirements.txt
	venv/bin/python -m pip install -r functions/check_metadata/requirements.txt
	venv/bin/python -m pip install -r functions/load_data/requirements.txt
	venv/bin/python -m pip install -r requirements.txt

unit:
	venv/bin/python -m unittest discover -s tests/

lint:
	prospector
